package SensorTester;

import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;

public class MidiInputReceiver implements Receiver {
	public String name;
	int Xrel = 0, Yrel = 0, Zrel = 0, Xabs = 0, Yabs = 0, Zabs = 0;
	int addr = 0;

	public MidiInputReceiver(String name) {
		this.name = name;
	}

	public void send(MidiMessage msg, long timeStamp) {
		byte[] aMsg = msg.getMessage();
		int[] arr = new int[aMsg.length];
		for (int i = 0; i < aMsg.length; i++) {
			arr[i] = (int) aMsg[i];
		}
		if (arr[1] == 20) {
			Xrel = arr[2];
		}
		else if (arr[1] == 21) {
			Yrel = arr[2];
		}
		else if (arr[1] == 22) {
			Zrel = arr[2];
		}
		else if (arr[1] == 102) {
			Xabs = arr[2];
		}
		else if (arr[1] == 103) {
			Yabs = arr[2];
		}
		else if (arr[1] == 104) {
			Zabs = arr[2];
		}
		midiCC.setXrel(Xrel);
		midiCC.setYrel(Yrel);
		midiCC.setZrel(Zrel);
		midiCC.setXabs(Xabs);
		midiCC.setYabs(Yabs);
		midiCC.setZabs(Zabs);
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
	}
}