package SensorTester;

import javax.sound.midi.*;
import java.util.ArrayList;
import java.util.List;
import java.io.*;

public class midiIn {
	String[] portNames = new String[20];

	
	public midiIn() {
	}

	int selectedPort;

	public void deviceInfo() {
		MidiDevice device;
		MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
		int counter = 0;
		
		for (int i = 0; i < infos.length; i++) {
			try {
				device = MidiSystem.getMidiDevice(infos[i]);
				List<Transmitter> transmitters = device.getTransmitters();
				Transmitter X = device.getTransmitter();
				System.out.println((counter + 1) + " = " + device.getDeviceInfo() + " Was Opened ");
				counter++;
			}
			catch (MidiUnavailableException e) {
			}
		}
	}

	public void createReceiver(int p) {
		int selPort = p;
		int counter = 0;
		MidiDevice device;
		MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
		
		for (int i = 0; i < infos.length; i++) {
			try {
				device = MidiSystem.getMidiDevice(infos[i]);
				List<Transmitter> transmitters = device.getTransmitters();
				int ports = infos.length;
				if (counter == selPort) {
					for (int j = 0; j < transmitters.size(); j++) {
						// create a new receiver
						transmitters.get(j).setReceiver(
								// using my own MidiInputReceiver
								new MidiInputReceiver(device.getDeviceInfo().toString()));
					}
					Transmitter trans = device.getTransmitter();
					trans.setReceiver(new MidiInputReceiver(device.getDeviceInfo().toString()));
					// open each device
					device.open();
				}
				counter++;
			}
			catch (MidiUnavailableException e) {
			}
		}
		
	}
//tried to write my own class. I thought the send method handles an MidiEvents sent to it
}