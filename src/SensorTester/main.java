package SensorTester;

import javax.sound.sampled.LineUnavailableException;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class main {
	private static toneGen m_thread;

	public static void main(String args[]) throws LineUnavailableException, InterruptedException {
		
		m_thread = new toneGen();
		int cmd = 1;
		boolean repeat = true;
		int selectedPort;
		
	
		midiIn MMC;
		System.out.println();
		System.out.println();
		System.out.println("mictic sensor tester");
		System.out.println();
		System.out.println();
		TimeUnit.MILLISECONDS.sleep(500);
		
		//midi port selection
		System.out.println("select port: ");	
		MMC = new midiIn();
		MMC.deviceInfo();
		Scanner input = new Scanner(System.in);
		selectedPort = input.nextInt();
		MMC.createReceiver(selectedPort);
		System.out.println();
		TimeUnit.MILLISECONDS.sleep(200);
		
		//parameter selection
		System.out.println("Select tone parameter: ");
		System.out.println();
		System.out.println("1 = X relative (wrist)");
		System.out.println("2 = Y relative (up-down)");
		System.out.println("3 = Z relative (left-right)");
		System.out.println("4 = X absolute (wrist)");
		System.out.println("5 = Y absolute (up-down)");
		System.out.println("6 = Z absolute (left-right)");
		
		try (Scanner input2 = new Scanner(System.in)) {
			String userInput = input2.nextLine();
			
			switch (userInput) {
			case "1": cmd = 1; System.out.println("selected: Xrel");
				break;
			case "2":	cmd = 2; System.out.println("selected: Yrel");
				break;
			case "3":	cmd = 3; System.out.println("selected: Zrel");
				break;
			case "4": cmd = 4; System.out.println("selected: Xabs");
				break;
			case "5": cmd = 5; System.out.println("selected: Yabs");
				break;
			case "6": cmd = 6; System.out.println("selected: Zabs");
				break;
			default: System.out.println("invalid input");
			}
			input.close();
		}
		
		
		midiCC.setToneParam(cmd);
		m_thread.start();
		
		//continuously output sensor values
		while (repeat) {
			midiCC.printData();
			TimeUnit.MILLISECONDS.sleep(10);
		}
		
	}
}