package SensorTester;

import java.util.Collections;

public class midiCC {
	static int Xrel = 0, Yrel = 0, Zrel = 0, Xabs = 0, Yabs = 0, Zabs = 0, param = 0;

	
	public midiCC() {
	}

	public static void setXrel(int xr) {
		Xrel = xr;
	}

	public static void setYrel(int yr) {
		Yrel = yr;
	}

	public static void setZrel(int zr) {
		Zrel = zr;
	}

	public static void setXabs(int xa) {
		Xabs = xa;
	}

	public static void setYabs(int ya) {
		Yabs = ya;
	}

	public static void setZabs(int za) {
		Zabs = za;
	}

	public static void setToneParam(int tp) {
		param = tp;
	}

	//sensor values output
	public static void printData() {
		
		StringBuilder outString = new StringBuilder(140);
		StringBuilder selector = new StringBuilder(140);
		outString.append('\r').append("    Xrel").append(String.format("%4d", Xrel)).append("    Yrel")
				.append(String.format("%4d", Yrel)).append("    Zrel").append(String.format("%4d", Zrel)).append("    Xabs")
				.append(String.format("%4d", Xabs)).append("    Yabs").append(String.format("%4d", Yabs)).append("    Zabs")
				.append(String.format("%4d", Zabs));
		selector.append("  ").append(String.join("", Collections.nCopies(midiCC.getToneParam(), "           "))).append("^");
		System.out.println(new String(new char[70]).replace("\0", "\r\n"));
		System.out.println(outString);
		System.out.println();
		System.out.println(selector);
	}

	public static int getXrel() {
		return Xrel;
	}

	public static int getYrel() {
		return Yrel;
	}

	public static int getZrel() {
		return Zrel;
	}

	public static int getXabs() {
		return Xabs;
	}

	public static int getYabs() {
		return Yabs;
	}

	public static int getZabs() {
		return Zabs;
	}

	//sensor value to tone frequency
	public static int getToneFreq() {
		double value = 0;
		int output = 0;
		if (param == 1) {
			value = Xrel;
		}
		else if (param == 2) {
			value = Yrel;
		}
		else if (param == 3) {
			value = Zrel;
		}
		else if (param == 4) {
			value = Xabs;
		}
		else if (param == 5) {
			value = Yabs;
		}
		else if (param == 6) {
			value = Zabs;
		}
		else {
			value = 0;
		}
		output = (int) ((value * 5.19) + 220);
		return output;
	}

	public static int getToneParam() {
		return param;
	}
}
